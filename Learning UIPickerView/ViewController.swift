//
//  ViewController.swift
//  Learning UIPickerView
//
//  Created by Liron Yehoshua on 7/19/15.
//  Copyright (c) 2015 Cookies. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //DATA
    let componentCount = 2;
    let israelCities  = ["Tel Aviv","Kfar Savta","Eilat","Jerusalem"];
    let usCities = ["New York","Chicago","Los Angeles"];
    let coutriesArray = ["ISRAEL","US"];
    var allCountriesAndCities: [Array<String>]!;
    
    enum Pickers: String{
        case Picker_Countries="countries",Picker_Cities="cities";
    }
    
    //VIEWS
    var picker: UIPickerView!;
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // "LOAD CALCULATED DATA"
        allCountriesAndCities = [israelCities,usCities];
        
        picker = UIPickerView();
        picker.center = view.center;
        
        picker.dataSource = self;
        picker.delegate = self;
        
        
        
        view.addSubview(picker);
        
        
    }
    // returning number of components (pickers)
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return componentCount;
    }
    
    // returning numbers of rows for each component
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch (component){
        case Pickers.Picker_Countries.hashValue:
            return allCountriesAndCities.count;
        case Pickers.Picker_Cities.hashValue:
            let SelectedCountryRow = pickerView.selectedRowInComponent(component);
            
            return allCountriesAndCities[SelectedCountryRow].count-1;
            
        default:
            return 0;
            
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        switch (component){
            
        case Pickers.Picker_Countries.hashValue:
            return coutriesArray[row];
            
        case Pickers.Picker_Cities.hashValue:
            
            return allCountriesAndCities[picker.selectedRowInComponent(0)][row];
            
        default:
            return "error";
            
        }
        
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(component == 0 ){
            picker.reloadAllComponents();
            
        }else{
            println("The selected city is \(allCountriesAndCities[pickerView.selectedRowInComponent(0)][pickerView.selectedRowInComponent(1)])")
        }
    }
    
    
    
    
    
    
}





